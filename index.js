// storing multiple values using variables
let student1 = '2020-01-17'
let student2 = '2020-01-20'
let student3 = '2020-01-25'
let student4 = '2020-02-25'

console.log(student1)
console.log(student2)
console.log(student3)

// storing multiple values in an array
const studentNumbers = ['2020-01-17','2020-01-20','2020-01-25','2020-02-25']
console.log(studentNumbers)

// outputs value of element in index 1 of studentNumbersArray
// all arrays starts at index 0
// syntax: arrayName[index of value we want to output]
console.log(studentNumbers[1])
console.log('Index 3 of studentNumbers: ', studentNumbers[3])
console.log(studentNumbers[4])
// returns undefined since there is no value defined for fourth
// index of studentNumbers array

// arrays are declared using the square brackets aka Array Literals
// each data stored inside an array is called an array element

const emptyArray = [] 
const grades = [75, 85.5, 92, 94]
const computerBrands = ['Acer', 'Asus', 'Lenovo','Apple','Redfox','Gateway']

console.log('Empty Array Sample: ', emptyArray);
console.log(grades)
console.log('Computer Brands Array: ', computerBrands)

// all elements inside an array should have the same data type
// all elements inside an array should be related with each other
// not ideal:
const mixedArr = [12, 'Asus', undefined, null, {}]

const fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit']
console.log('Value of fruits array before push()', fruits)

// push() function adds an element at the end of an array
fruits.push('Mango')
console.log('Value of fruits array after push()', fruits)

console.log('Value of fruits before pop()', fruits)

// removes last element in array
fruits.pop()
console.log('Value of fruits after pop()', fruits)

console.log('Value of fruits before unshift()', fruits)

// unshift() adds one or more elements at the beginning of array
// adds strawberry to the beginning of fruits array
fruits.unshift('strawberry')
console.log(fruits)
fruits.unshift('banana', 'raisins')
console.log(fruits)

// shift() removes first element in our array

let removedFruit = fruits.shift()
console.log('You successfully removed the fruit', removedFruit)
console.log(fruits)

// reverse() reverses order in array



const tasks = [
	'drink html',
	'eat JS',
	'inhale CSS',
	'bake sass'
]

console.log('Value of tasks before sort()', tasks);
tasks.sort()
console.log('Value of tasks after sort()', tasks)

const oddNumbers = [1, 3, 9, 5, 7]

// arranges elements in alphanumeric sequence
console.log('Value of oddNumbers before sort()', oddNumbers)
oddNumbers.sort()
console.log('Value of oddNumbers after sort()', oddNumbers)

const countries = ['US', 'PH', 'CAN', 'SG', 'PH']
// indexOf() finds index of a given element where it is FIRST found
let indexCountry = countries.indexOf('PH')
console.log('Index of PH:', indexCountry)

// lastIndexOf() finds index of a given element where it is LAST found
let lastIndexCountry = countries.lastIndexOf('PH')
console.log('Last Instance of PH:', lastIndexCountry)

console.log(countries)
// toString() converts an array into a single value that is separated by a comma
console.log(countries.toString())

const subTasksA = ['drink html', 'eat JS']
const subTasksB = ['inhale css', 'breathe sass']
const subTasksC = ['get git', 'be node']

// concat() joins 2 or more arrays
const subTasks = subTasksA.concat(subTasksB, subTasksC)
console.log(subTasks)

// join() converts array into single value, separated by specified char
console.log(subTasks.join('-'))
console.log(subTasks.join('@'))
console.log(subTasks.join('x'))


const users = ['blue', 'alexis', 'bianca', 'nikko', 'adrian']

console.log(users[0])
console.log(users[1])

// syntax: arrayName.forEach
// arrayName.forEach(function(eachElement){
// 	console.log(eachElement)
// })


users.forEach(function(user) {
	// console.log('Name of Student: ', user)

	if(user == 'blue') {
		console.log('Name of Student: ', user)
	}
})

// Mini Activity
const numberList = [1,2,3,4,5,6,7,8,9]
numberList.forEach(function(number){
	if (number%2 == 0 ) {
		console.log(number)
	}
})

const numbersData = numberList.map(function(number){
	return number * number
})

console.log(numbersData)
console.log(numberList)

// multidimensional arrays
const chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4']
]

console.log(chessBoard)
console.log(chessBoard[0])
console.log(chessBoard[0][1])

/* opening for multi line comment

closing for multi line comment
*/ 


// session activity
const students = []

function addStudent(name){

}

function countStudents (){
	// count number of students inside students array
}

function printStudents (){
	// sorts array in alphanumeric order

	// logs each student name in browser (using forEach())

}

addStudent('Carlo')
addStudent('Adrian')
(countStudents())
(printStudents())







